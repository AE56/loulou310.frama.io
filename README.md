
# Site internet pour la platforme Parcoursup®

Ce site internet sert de CV pour mon inscription sur la platforme Parcoursup®, et présente les différents projets en informatique que j'ai pu réaliser. Le site internet est disponibles aux adresses suivantes : https://loulou310.frama.io/ (lien de l'instance) ou https://xotak.me (mon domaine)

## Développement

Le site est réalisé avec le module Python mkdocs et le theme material. Pour modifier le site, il faut d'abbord installer les dépendances.

Dans un terminal, après avoir récupéré les sources, il faut lancer la commande 

```sh
pip install -r requirements.txt
```

Pour lancer le serveur de développement, on peut lancer la commande `mkdocs serve` dans le dossier des sources. Ce serveur détectera les changements apportés aux fichier et rechagera automatiquement les navigateurs connectés.

Testé avec Python 3.10 et 3.11

Avant de contribuer, veuillez lire la note de contribution et accepter les termes de la license.


Toutes les marques et noms de marques mentionnés appartiennent à leur propriétaires respectifs. Ce site internet et le code source associé n'est en aucun cas associé avec ces entreprises.