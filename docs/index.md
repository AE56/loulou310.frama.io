# 🏡 Accueil

Bienvenue sur ma page.
Sur cette page vous pourrez trouver les différents projets en programmation, que ce soit personnellement ou en cours de NSI. Vous pouvez naviguer a travers ce site grâce a la barre de navigation en haut de la page.


## A propos de moi

Vous devez surement le savoir, mais je m'appelle Jean-Louis Emeraud, j'ai 15 ans et je suis acctuellement en classe de Terminale. Je passe un bac général avec les spécialités Mathématiques et Numérique et Sciences Informatique (NSI), et l'option Mathématiques Expertes. J'ai suivi l'option Management et Gestion en 2nde. Je souhaite faire une B.U.T Informatique afin de poursuivre vers des études de cyber-sécurité. Je souhaite également faire des études de sysadmin car le domaine m'interesse.

Je maitrise le Python, le développement web (HTML, CSS et JavaScript), le développement avec node.js, et quelques librairies comme discord.js, et le C++. Je possède également des bases en Java, React et j'ai fait un peu de VB.NET il y a longtemps

J'ai également quelques compétences hors informatique. Mon père étant couvreur, j'ai été amené a l'aider sur différents chantiers. Je bricole aussi de temps en temps à la maison. Nous construisons également une résidence secondaire dans le Finistère, et jusqu'a présent j'ai participé à la maçonerie. 

Je fait partie de l'équipe[^1] Backstage depuis 3 ans. Le but de cet équipe est de réalister des instalations matérieles et de gérér les effets lumineux et le mixage son. Cette équipe nécessite une grande coordination entre les différents membres pour gérer les instalations facilement, et pour avoir une bonne synchronisation entre les différents postes.


Contenu mis à jour le `17/02/2023`  
Backend mis à jour le `03/03/2023`  (Màj/Pipelines/Configutation)


[^1]: Voir [ce lien](https://fr.m.wikipedia.org/wiki/Coll%C3%A8ge-lyc%C3%A9e-pr%C3%A9pa_Saint-Fran%C3%A7ois-Xavier_de_Vannes#Les_%C3%A9quipes_du_lyc%C3%A9e)