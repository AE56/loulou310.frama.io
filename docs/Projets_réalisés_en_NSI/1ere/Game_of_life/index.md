---
tags:
    - Python
    - Jeu

# Game of life

Le but de ce projet était de recréer le "Game of life" inventé par Conway en Python pour une exécution dans la console. Le programme permet d'observer une simulation, avec les cellules de départ codé en dur.

Voici des extraits de code : 

=== "Comptage du nombre de voisins"
    ```py
    def nbr_voisins(grille, i, j):
        nb=0
        if i!=0 :
            if grille[i-1][j]   == VIVANT: nb+=1
        if i!=0 and j!=0:
            if grille[i-1][j-1] == VIVANT: nb+=1
        if j!=0:
            if grille[i][j-1] == VIVANT: nb+=1
        if j!=largeur:
            if grille[i][j+1] == VIVANT: nb+=1
        if i!=largeur and j!=largeur:
            if grille[i+1][j+1] == VIVANT: nb+=1
        if i!= largeur:
            if grille[i+1][j] == VIVANT: nb+=1
        if i!= largeur and j!=0:
            if grille[i+1][j-1] == VIVANT : nb+=1
        if i!=0 and j !=largeur:
            if grille[i-1][j+1] == VIVANT : nb+=1
        return nb
    
    ```
    
=== "Boucle principale"
    ```py
    while True:
        chaine = grille_en_chaine(grille)
        print(chaine)
        step = grille_vide()
        for i in range(0, LARGEUR):
            for j in range(0, LARGEUR):
                nVoisins = nbr_voisins(grille,i,j)
                if grille[i][j] == VIVANT :
                    if nVoisins <= 1 or nVoisins >= 4: step[i][j]=MORT
                    else : step[i][j] = VIVANT
                else : 
                    if nVoisins == 3 : step[i][j]=VIVANT
        if grille == step: break
        grille = step
        os.system("cls")
        print(grille_en_chaine(grille))
        sleep(0.5)
    ```