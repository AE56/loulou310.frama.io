---
tags:
    - Cyberdéfense
---

# CTFs

Pendant ma 1ère et Terminale, j'ai participé a différents CTFs.

## Midnight Flag - Infektion. Edition 2022

- Equipe : Masterhaxxor
- Membres : 2
- Rang : 154èmes
- Points : 517
- Flags : Meduzom I + Trailer

## NoBrackets. Edition 2022

- Equipe : Les galériens
- Membres : 2+2 personnes inconues
- Rang : 10ème Jour 1

## TryHackMe

- Niveau : 7
- Points : 3254
- Profil : [https://tryhackme.com/p/UnityXotak](https://tryhackme.com/p/UnityXotak)

## Root-Me

- Points : 70
- Profil : [https://www.root-me.org/loulou310-417929](https://www.root-me.org/loulou310-417929)