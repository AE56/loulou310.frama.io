---
tags:
    - Node.JS
    - Markdown
    - CI/CD
    - Web
---

# Site vitrine

Toujours dans le web, ma mère s'installe à son compte en tant que sophrologue et naturopathe. Mon rôle dans tout ceci ? J'ai pour tâche de réaliser comme page web un site vitrine avec une fonctionalité de blog. Pour ce faire, j'ai décidé de ne pas utiliser mkdocs comme j'aurais fait d'habitude, mais j'utilise un tout autre outil. J'ai opté pour Docusaurus. Cela change énormément, déja la partie configuration du site qui est plus compliquée, et la pipeline qui utilise node et non pas python. Le repo est accessible [ici](https://framagit.org/AE56/AE56.frama.io). Le site est actuellement vide, mais est déja configuré, ainsi que le domaine.