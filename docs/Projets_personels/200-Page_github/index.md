---
tags:
    - Python
    - Markdown
    - CI/CD
    - Web
---

# Page GitHub

Ce projet est né du conseil de mon professeur de NSI : déposer un lien vers mon compte Github sur ParcoursSup. 


Le but de ce site est donc de présenter brièvement mes projets personnels en programmation.

Ce site devait en premier lieu être un blog réalisé avec un CMS. Cependant, mon site s'est fait attaquer à plusieurs reprises, et je trouvais également plus valorisant de le créer moi-même.

J'ai donc décider de l'héberger sur GitHub grâce à l'environnement GitHub Pages et de construire le site avec Bootstrap. Ayant discuté de ce projet avec mon professeur de NSI, il m'a conseillé de créer le site grâce à MkDocs.

Le projet a donc évolué, et est sous la forme actuelle, c'est à dire MkDocs avec le thème Material, et toujours hébergé sur GitHub.

Le 17 juin, le projet a été déplacé sur Framagit, dans le but de promouvoir le logiciel libre. 

Le 9 novembre, j'ai décidé d'envoyer le sitemap sur Google pour son indexation. 

Le 25 novembre, je me suis inscrit au programme étudiant proposé par GitHub, qui m'a permis d'obtenir un domaine. Le domaine que je possède actuellement est xotak.me

le 20 janvier 2023, après m'être battu pendant 2 mois avec mon DNS, le site est à nouveau référencé sur Google, mais cette fois, c'est le domaine qui est référencé. J'ai également migré la pipeline vers alpine.

Repo : <a href="https://framagit.org/loulou310/loulou310.frama.io">Lien</a>
