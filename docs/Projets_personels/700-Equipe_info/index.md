---
tags:
    - Autres
---

# Equipe Informatique

Dans le cadre du système des équipes de Saint François-Xavier, je participe à l'équipe Backstage en temps normal. Mais, depuis cet année, je participe égalmement aux activitées de l'équipe informatique, étant donné que je dois rester au lycée jusqu'a 17h30 dans un souci d'organisation. L'équipe possède différents projets comme la réalisation d'une IA ou d'un jeu avec le moteur Unity. Pour ma part j'interviens pour faire des "cours" à propos de l'utilisation d'un système Linux, dans ce cas EndeavourOS, et la cybersécurité. La partie cyber à été un peu mise de côté, pour être remplacée par du développement Web.

J'ai mis en place un site de ressources, réalisé encore une fois avec mkdocs, accessible [ici](https://loulou310.frama.io/equipe-info/)