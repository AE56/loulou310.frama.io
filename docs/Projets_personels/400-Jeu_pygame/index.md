---
tags:
    - Python
    - Jeu
---

# Jeux réalisés avec pygame

Le but de ce projet est de réaliser un ou plusieurs jeux avec pygame.

Le premier jeu est inspiré de Adventure Capitalist, en reprenant seulement le concept, gardant une interface très sommaire

Repo disponible [ici](https://framagit.org/loulou310/jeu-pygame)